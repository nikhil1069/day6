package com.hsbc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class MapExample {
	public static void main(String[] args) {
		//Map<String, String> capitals = new HashMap<>();
		//Map<String, String> capitals = new LinkedHashMap<>();
		Map<String, String> capitals = new TreeMap<>();

		capitals.put("MH", "mumbai");
		capitals.put("TN", "chennai");
		capitals.put("TL", "hyderabad");
		capitals.put("AP", "hyderabad");
		capitals.put("Punjab", "chandigarh");
		capitals.put("HY", "chandigarh");
		capitals.put("MH", "pune");
		
		System.out.println(capitals);
		
		Set<String> states = capitals.keySet();
		System.out.println(states);
		Collection<String> capital = capitals.values();
		System.out.println(capital);
		
		for(Entry<String, String> set : capitals.entrySet()) {
			System.out.println(set);
		}
	}
}
