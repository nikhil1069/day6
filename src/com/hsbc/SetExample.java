package com.hsbc;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetExample {
	public static void main(String[] args) {
		System.out.println("set example...");
		//Set<String> skills = new HashSet<>();
		//Set<String> skills = new LinkedHashSet<>();
		//Set<String> skills = new TreeSet<>();		//ascending order
		Set<String> skills = new TreeSet<>(Collections.reverseOrder());		//descending order
		skills.add("java");
		skills.add("javascript");
		skills.add("aws");
		skills.add("angular");
		skills.add("react");
		skills.add("java");
		skills.add("javascript");
		skills.add("aws");
		skills.add("angular");
		System.out.println(skills);
		for(String s: skills) {
			System.out.println(s);
		}
		
		System.out.println("-------------------------------");
		Iterator<String> it = skills.iterator();
		while(it.hasNext()) {
			String temp = it.next();
			if(temp.equals("aws")) {
				it.remove();
			}else {
				System.out.println(temp);
			}
			
		}
	}
}
