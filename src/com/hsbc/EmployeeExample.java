package com.hsbc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class EmployeeExample {
	public static void main(String[] args) {
		Set<Employee> empSet = new HashSet<>();
		
		empSet.add(new Employee(101, "adm101", 123));
		empSet.add(new Employee(102, "bdm101", 54));
		empSet.add(new Employee(103, "cdm101", 67));
		empSet.add(new Employee(104, "ddm101", 23));
		empSet.add(new Employee(105, "ee", 567));
		empSet.add(new Employee(105, "ee", 567));
		System.out.println(empSet);
		
		for(Employee e: empSet) {
			System.out.println(e.hashCode());
		}
	}
}