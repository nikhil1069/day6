package com.accenture.service;

import java.util.List;

import com.accenture.dao.EmployeeDao;
import com.accenture.dao.EmployeeDaoImpl;
import com.accenture.model.Employee;

public class EmployeeServiceImpl implements EmployeeService {

	EmployeeDao empDao = new EmployeeDaoImpl();
	private int generateEmpId() {
		return (int) (Math.round(Math.random()*99999));
	}
	@Override
	public boolean add(Employee employee) {
		//write logic to create employee id
		employee.setId(generateEmpId());
		return empDao.addToDb(employee);
	}

	@Override
	public List<Employee> displayAll() {
		return empDao.displayAllFromDb();
	}

	@Override
	public boolean delete(int id) {
		return empDao.deleteFromDb(id);
	}

	@Override
	public boolean update(int id, Employee emp) {
		return false;
	}
	 
	
}
