package com.accenture.service;

import java.util.List;

import com.accenture.model.Employee;

public interface EmployeeService {
	public boolean add(Employee employee);
	public List<Employee> displayAll();
	public boolean delete(int id);
	public boolean update(int id, Employee emp);
}
