package com.accenture.ui;

import java.util.List;
import java.util.Scanner;

import com.accenture.model.Employee;
import com.accenture.service.EmployeeService;
import com.accenture.service.EmployeeServiceImpl;

public class UI {
	public static void main(String[] args) {
		//add employee
		//display employee
		//delete employee
		//update employee
		//create object of service
		EmployeeService empService = new EmployeeServiceImpl();
		boolean status = empService.add(new Employee("dm101", 123));
		boolean status1 = empService.add(new Employee("dm102", 65));
		boolean status2 = empService.add(new Employee("dm103", 78));
		boolean status3 = empService.add(new Employee("dm104", 23));
		if(status) {
			System.out.println("successfully added employee in the database...");
		}else {
			System.err.println("Not able to employee in database...");
		}
		if(status1) {
			System.out.println("successfully added employee in the database...");
		}else {
			System.err.println("Not able to employee in database...");
		}
		if(status2) {
			System.out.println("successfully added employee in the database...");
		}else {
			System.err.println("Not able to employee in database...");
		}
		if(status3) {
			System.out.println("successfully added employee in the database...");
		}else {
			System.err.println("Not able to employee in database...");
		}
		//get all employee from database...
		List<Employee> empList = empService.displayAll();
		System.out.println(empList);
		//delete employee
		Scanner scan = new Scanner(System.in);
		System.out.println("please enter emp id which you want to delete");
		int id = scan.nextInt();
		status = empService.delete(id);
		if(status) { System.out.println("employee deleted");}
		else {System.out.println("not able to delete employee"); }
		System.out.println("-------- after deletion-----------");
		empList = empService.displayAll();
		System.out.println(empList);
	}
}
