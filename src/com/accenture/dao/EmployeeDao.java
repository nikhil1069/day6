package com.accenture.dao;

import java.util.List;

import com.accenture.model.Employee;

public interface EmployeeDao {
	public boolean addToDb(Employee employee);
	public List<Employee> displayAllFromDb();
	public boolean deleteFromDb(int id);
	public boolean updateToDb(int id, Employee emp);
}
