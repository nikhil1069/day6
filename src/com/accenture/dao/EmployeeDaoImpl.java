package com.accenture.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.accenture.model.Employee;

public class EmployeeDaoImpl implements EmployeeDao {

	private List<Employee> empList = new ArrayList<>(); // database for now

	public EmployeeDaoImpl() {

	}

	@Override
	public boolean addToDb(Employee employee) {
		return empList.add(employee);
	}

	@Override
	public List<Employee> displayAllFromDb() {
		return empList;
	}

	@Override
	public boolean deleteFromDb(int id) {
		Iterator<Employee> empIterator = empList.iterator();
		boolean status = false;
		while (empIterator.hasNext()) {
			Employee temp = empIterator.next();
			if (temp.getId() == id) {
				status = true;
				empIterator.remove();
				break;
			}
		}
		return status;
	}

	@Override
	public boolean updateToDb(int id, Employee emp) {
		return false;
	}

}
